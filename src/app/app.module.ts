import {HttpClientModule} from '@angular/common/http';
import {
  Compiler,
  CompilerFactory,
  COMPILER_OPTIONS,
  NgModule
} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {JitCompilerFactory} from '@angular/platform-browser-dynamic';
import {setAngularJSGlobal, UpgradeModule} from '@angular/upgrade/static';
import {Instruction} from '@rayon/instruction';
import {MeetingModule} from '@rayon/meeting';
import {Program} from '@rayon/program';
import {
  CdpBpmComponentsModule,
  TaskWrapperComponent
} from '@reinform-cdp/bpm-components';
import {CoreModule} from '@reinform-cdp/core';
import {FileResourceServiceProvider} from '@reinform-cdp/file-resource';
import {CdpLoggerModule} from '@reinform-cdp/logger';
import {SolarResourceServiceProvider} from '@reinform-cdp/search-resource';
import {PageNotFoundComponent, SkeletonModule} from '@reinform-cdp/skeleton';
import '@uirouter/angular-hybrid';
import {UIRouterUpgradeModule} from '@uirouter/angular-hybrid';
import {Transition, TransitionService, UrlService} from '@uirouter/core';
import * as angular from 'angular';
import 'hammerjs';
import 'mousetrap';
import {FullCalendarModule} from 'ng-fullcalendar';
import {DependenciesTreeComponent} from './components/dependencies-tree/dependencies-tree.component';
import {MainComponent} from './components/main/main.component';

setAngularJSGlobal(angular);
declare let document: any;

export function createCompiler(compilerFactory: CompilerFactory) {
  return compilerFactory.createCompiler();
}

const ng1Module = angular.module('rayon.portal', [
  'ui.router',
  'ui.router.upgrade',
  'cdp.skeleton',
  'cdp.core',
  'cdp.bpm.components',
  'cdp.file.resource',
  'cdp.logger'
]);

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    UpgradeModule,
    UIRouterUpgradeModule.forRoot({
      states: [
        {
          name: 'app.versions',
          url: '/versions',
          component: DependenciesTreeComponent
        },
        {
          name: 'app.execution',
          url: '/execution/:system/:taskId',
          component: TaskWrapperComponent
        } /*,
        {
          name: 'app.main',
          url: '/main',
          component: MainComponent
        }*/,
        {
          name: '404',
          url: '/*path',
          component: PageNotFoundComponent
        }
      ]
    }),
    SkeletonModule.forRoot(),
    CoreModule.forRoot('mr', '/main/', 'Главная', '/app/mr/bpm'),
    CdpBpmComponentsModule.forRoot(),
    Program.forRoot(),
    Instruction.forRoot(),
    MeetingModule.forRoot(),
    CdpLoggerModule.forRoot(),
    FullCalendarModule
  ],
  declarations: [MainComponent, DependenciesTreeComponent],
  entryComponents: [DependenciesTreeComponent],
  providers: [
    {provide: COMPILER_OPTIONS, useValue: {useJit: true}, multi: true},
    {
      provide: CompilerFactory,
      useClass: JitCompilerFactory,
      deps: [COMPILER_OPTIONS]
    },
    {provide: Compiler, useFactory: createCompiler, deps: [CompilerFactory]}
  ]
})
export class AppModule {
  constructor(private upgrade: UpgradeModule) {}

  ngDoBootstrap() {
    this.upgrade.bootstrap(document, ['rayon.portal'], {strictDi: false});
  }
}

ng1Module.config([
  '$urlServiceProvider',
  ($urlService: UrlService) => $urlService.deferIntercept()
]);

ng1Module.config([
  '$stateProvider',
  '$urlRouterProvider',
  ($stateProvider: any, $urlRouterProvider: any) => {
    $urlRouterProvider.otherwise(($injector, $location) => {
      // const $state = $injector.get('$state');
      // $state.go('app.versions');
      window.location.href = '/main/';
    });
  }
]);

ng1Module.config([
  'cdpFileResourceServiceProvider',
  (fileResourceServiceProvider: FileResourceServiceProvider) => {
    fileResourceServiceProvider.root = '/filestore/v1';
  }
]);

ng1Module.config([
  'cdpSolarResourceServiceProvider',
  (cdpSolarResourceServiceProvider: SolarResourceServiceProvider) => {
    cdpSolarResourceServiceProvider.root = '/app/mr/search';
  }
]);
// перезагрузать, когда обновится кеш сервис-воркера ( workbox). смотри index.html
ng1Module.run([
  '$transitions',
  '$q',
  function($transitions: TransitionService, $q: ng.IQService) {
    $transitions.onSuccess({}, function(trans: Transition) {
      if (window.sessionStorage && window.sessionStorage['cdp-reload']) {
        setTimeout(() => {
          delete window.sessionStorage['cdp-reload'];
          window.location.reload();
          console.log('New version arrived');
        }, 0);
      } else {
        trans.options().reload = false;
      }
    });
  }
]);
