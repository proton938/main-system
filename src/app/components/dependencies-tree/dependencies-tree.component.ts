import {Component, OnInit} from '@angular/core';
import * as d3 from 'd3';
import 'd3-shape';
import {TreeLayout} from 'd3-hierarchy';

declare let $: any;
declare let require: any;

@Component({
    selector: 'dependencies-tree',
    templateUrl: './dependencies-tree.component.html',
    styleUrls: ['./dependencies-tree.component.scss']
})
export class DependenciesTreeComponent implements OnInit {
    private treemap: TreeLayout<any>;

    private svg: any;
    private root: any;
    private duration: number = 750;

    ngOnInit(): void {
        // Set the dimensions and margins of the diagram
        let margin = {top: 20, right: 90, bottom: 30, left: 90},
            width = $('#tree-container').width() - margin.left - margin.right,
            height = 800 - margin.top - margin.bottom;

        // append the svg object to the body of the page
        // appends a 'group' element to 'svg'
        // moves the 'group' element to the top left margin
        this.svg = d3.select('#tree-container').append('svg')
            .attr('width', width + margin.right + margin.left)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

        let data = require('../../../data/plain-versions.json');

        let treeData = {name: 'app', dependencies: data};

        // declares a tree layout and assigns the size
        this.treemap = d3.tree().size([height, width]);

        // Assigns parent, children, height, depth
        this.root = d3.hierarchy(treeData, function (d) {
            return d.dependencies;
        });
        this.root.x0 = height / 2;
        this.root.y0 = 0;
        this.root.id = 'uniqId';
        this.root.children.forEach(el => {
            this.setUniqueId(el);
        });

        // Collapse after the second level
        this.root.children.forEach(el => this.collapse(el));

        this.update(this.root);

    }

    // Creates a curved (diagonal) path from parent to the child nodes
    diagonal(s, d) {
        return `M ${s.y} ${s.x}
            C ${(s.y + d.y) / 2} ${s.x},
              ${(s.y + d.y) / 2} ${d.x},
              ${d.y} ${d.x}`;

    }

    // Toggle children on click.
    click(d) {
        if (!d.children && !d._children) return;

        if (d.children) {
            d._children = d.children;
            d.children = null;
        } else {
            d.children = d._children;
            d._children = null;
        }
        this.update(d);
    }

    collapse(d) {
        if (d.children) {
            d._children = d.children;
            d._children.forEach(el => this.collapse(el));
            d.children = null;
        }
    }

    update(source) {

        // Assigns the x and y position for the nodes
        let treeData = this.treemap(this.root);

        // Compute the new tree layout.
        let nodes = treeData.descendants(),
            links = treeData.descendants().slice(1);

        // Normalize for fixed-depth.
        nodes.forEach(function (d) {
            d.y = d.depth * 360;
        });

        // ****************** Nodes section ***************************

        // Update the nodes...
        let node = this.svg.selectAll('g.node')
            .data(nodes, function (d) {
                return d.id;
            });

        // Enter any new modes at the parent's previous position.
        let nodeEnter = node.enter().append('g')
            .attr('class', 'node')
            .attr('transform', function (d) {
                return 'translate(' + source.y0 + ',' + source.x0 + ')';
            })
            .on('click', el => this.click(el));

        // Add Circle for the nodes
        nodeEnter.append('circle')
            .attr('class', 'node')
            .attr('r', 1e-6)
            .style('fill', function (d) {
                return d._children ? 'lightsteelblue' : '#fff';
            });

        // Add labels for the nodes
        nodeEnter.append('text')
            .attr('dy', '.35em')
            .attr('x', function (d) {
                return d.children || d._children ? -13 : 13;
            })
            .attr('text-anchor', function (d) {
                return d.children || d._children ? 'end' : 'start';
            })
            .text(function (d) {
                return d.data.name;
            });

        // UPDATE
        let nodeUpdate = nodeEnter.merge(node);

        // Transition to the proper position for the node
        nodeUpdate.transition()
            .duration(this.duration)
            .attr('transform', function (d) {
                return 'translate(' + d.y + ',' + d.x + ')';
            });

        // Update the node attributes and style
        nodeUpdate.select('circle.node')
            .attr('r', 5)
            .style('fill', function (d) {
                return d._children ? 'lightsteelblue' : '#fff';
            })
            .attr('cursor', 'pointer');


        // Remove any exiting nodes
        let nodeExit = node.exit().transition()
            .duration(this.duration)
            .attr('transform', function (d) {
                return 'translate(' + source.y + ',' + source.x + ')';
            })
            .remove();

        // On exit reduce the node circles size to 0
        nodeExit.select('circle')
            .attr('r', 1e-6);

        // On exit reduce the opacity of text labels
        nodeExit.select('text')
            .style('fill-opacity', 1e-6);

        // ****************** links section ***************************

        // Update the links...
        let link = this.svg.selectAll('path.link')
            .data(links, function (d) {
                return d.id;
            });

        // Enter any new links at the parent's previous position.
        let linkEnter = link.enter().insert('path', 'g')
            .attr('class', 'link')
            .attr('d', (d) => {
                let o = {x: source.x0, y: source.y0};
                return this.diagonal(o, o);
            });

        // UPDATE
        let linkUpdate = linkEnter.merge(link);

        // Transition back to the parent element position
        linkUpdate.transition()
            .duration(this.duration)
            .attr('d', (d) => {
                return this.diagonal(d, d.parent);
            });

        // Remove any exiting links
        let linkExit = link.exit().transition()
            .duration(this.duration)
            .attr('d', (d) => {
                let o = {x: source.x, y: source.y};
                return this.diagonal(o, o);
            })
            .remove();

        // Store the old positions for transition.
        nodes.forEach(function (d: any) {
            d.x0 = d.x;
            d.y0 = d.y;
        });

    }

    guidGenerator() {
        let S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + '-' + S4() + '-' + S4() + '-' + S4() + '-' + S4() + S4() + S4());
    }

    private setUniqueId(el: any) {
        el.id = this.guidGenerator();
        if (el.children) {
            el.children.forEach(el => this.setUniqueId(el));
        }
    }

}
